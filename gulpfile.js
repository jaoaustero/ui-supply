const gulp = require('gulp');

const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const minCss = require('gulp-minify-css');
const rename = require('gulp-rename');

const uglify = require('gulp-uglify-es').default;
const concat = require('gulp-concat-util');

const browserify = require('browserify')
const babelify = require('babelify')
const source = require('vinyl-source-stream')
const buffer = require('vinyl-buffer')

const jsSrc = 'build/js/index.js'
const jsDist = './'
const jsWatch = 'build/js/*.js'
const jsFiles = [jsSrc]
/*
    -- TOP LEVEL FUNCTIONS --
    gulp.task - Define tasks
    gulp.src - point to files to use
    gulp.dest - Points to folder to output
    gulp.watch - watch files and folders for changes
*/


/* ==================================================
  Task: Sass
 ================================================== */

gulp.task('sass', function(){
    return gulp.src('./build/scss/main.scss')

        // Output non-minified CSS file
        .pipe(sass({
            outputStyle:'expanded'
        }).on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(rename({ 
            prefix: 'ui-',
            basename: 'supply', 
            extname: '.css'
        }))
        .pipe(gulp.dest('./public/lib/uisupply/css/'))

        // Output the minified version
        .pipe(minCss())
        .pipe(rename({
            prefix: 'ui-', 
            basename: 'supply', 
            extname: '.min.css' 
        }))
        .pipe(gulp.dest('./public/lib/uisupply/css/'))
});



/* ==================================================
  Task: sass-pages
 ================================================== */
gulp.task('sass-pages', function(){
    return gulp.src('./build/scss/pages/*.scss')

        // Output non-minified CSS file
        .pipe(sass({
            outputStyle:'expanded'
        }).on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(rename({ 
            prefix: 'uis-',
            extname: '.css'
        }))
        .pipe(gulp.dest('./public/css/'))

        // Output the minified version
        .pipe(minCss())
        .pipe(rename({
            extname: '.min.css' 
        }))
        .pipe(gulp.dest('./public/css/'))
});



/* ==================================================
  Task: js
 ================================================== */

gulp.task('js', function() {
    gulp.src('./build/js/*.js')

        // Output non-minified version
        .pipe(concat('ui-supply.js'))
        .pipe(concat.header('var UIsupply = function(){\n'))
        .pipe(concat.footer('\n} \n\nvar UIs = new UIsupply();'))
        .pipe(gulp.dest('./public/lib/uisupply/js/'))

        // Output minified version
        .pipe(uglify())
        .pipe(rename({
            extname: '.min.js' }))
        .pipe(gulp.dest('./public/lib/uisupply/js/'))
});

gulp.task('sample', function(){
    jsFiles.map(function(entry){
        return browserify({
            entries: [entry]
        })
        .transform( babelify, {presets: ['env']})
        .bundle()
        .pipe(source(entry))
        .pipe(rename({ extname: '.min.js'}))
        .pipe( buffer() )
        .pipe(gulp.dest( jsDist ))
    });
});


/* ==================================================
  Task: watch
 ================================================== */

gulp.task('watch', () => {
    gulp.watch('./build/scss/**/*.scss', ['sass']);
    gulp.watch('./build/scss/pages/*.scss', ['sass-pages']);
    gulp.watch('./build/js/*.js', ['js']);
}); 