
    // ==================================================
    // Component: Nav
    // ==================================================

    const Nav = function(){
        /**
         * Constants
         * ==================================================
         */
        const selector = {
            nav_attr        : '[uis-nav-accordion]',
            nav_attr_name   :   'uis-nav-accordion'
        }

        const state = {
            open    : 'uis-open'
        }

        /**
         * Nav Accordion
         * When user clicks on the parent
         * 1. If the parent element has `href` it will prevent it.
         * 2. Get the attribute value
         * 3. Condition
         */
        $(document).on('click', selector.nav_attr + '> .uis-parent', function(e){
            /* 1 */
            e.preventDefault();
            
            /* 2 */
            const value = SplitParameter($(this).parent('ul.uis-nav').attr(selector.nav_attr_name));
            
            /* 3 */
            if(value[0] == 'multiple' && value[1] == 'true'){
                toggleNavSub($(this))
            }else{
                /**
                 * close the other siblings
                 */
                if($(this).siblings().hasClass(state.open)){
                    $(this).siblings().removeClass(state.open);
                    $(this).siblings().children('ul').slideToggle();
                }
                toggleNavSub($(this))
            }
        })

        function toggleNavSub(target){
            if(!$(target).hasClass(state.open)){
                $(target).addClass(state.open);
            }else{
                $(target).removeClass(state.open);
            }

            $(target).children('ul.uis-nav-sub').slideToggle();
        }
    }();
