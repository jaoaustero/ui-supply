    // ==================================================
    // Component: Offcanvas
    // ==================================================

    this.Offcanvas = function(){
        /**
         * Constants
         * ==================================================
         */
        const selector = {
            attr            : '[uis-offcanvas]',
            attr_name       : 'uis-offcanvas',
            bar             : 'uis-offcanvas-bar',
            container       : 'uis-offcanvas-container',
            page            : 'uis-offcanvas-page',
            offcanvas       : 'uis-offcanvas',

            option_attr     : '[uis-option]',
            option          : 'uis-option',

            offcanvas_close : 'uis-offcanvas-close',

            html            : 'html',
            body            : 'body'
        }

        const modifier = {
            bar                 : 'uis-offcanvas-bar-animation',
            container           : 'uis-offcanvas-container-animation',
            overlay             : 'uis-offcanvas-overlay',
            flip                : 'uis-offcanvas-flip',
            slide               : 'uis-offcanvas-slide',
            push                : 'uis-offcanvas-push',
            none                : 'uis-offcanvas-none'
        }
    
        const state = {
            open        : 'uis-open'
        }
    
        /**
         * The parameter will pass by the trigger function
         * 1. Split the parameter `helper.js`
         * 2. Put the return value in varible object
         * 3. Function
         */

        $(document).on('click', selector.attr, function(){
            
            /**
             * Note: To trigger this function you need to include the `uis-offcanvas as your attribute`
             * Two options for opening the offcanvas
             * A. Using by anchor using the attr `uis-offcanvas` it will get the `href` value.
             * B. Using by button it will get the value of `uis-offcanvas` attr.
             */

            /* A */
            if($(this).attr('href')){
                Open($(this).attr('href'));
            }
            /* B */
            else{
                /* 1 */
                const value = SplitParameter($(this).attr(selector.attr_name));

                /* 2 */
                const element = {
                    action  : value[0],
                    target  : value[1]
                };

                if(element.action == 'target'){
                    Open(element.target);
                }
            }
        });

        /*
         * Close sidebar
         * 1. Trigger by `uis-offcanvas-close` button
         * 2. Trigger by clicking outside of the offcanvas bar "If param has `bg-close: true`"
         * 3. Trigger by calling the function
         * 4. Trigger by pressing the `Esc` key "If param has `esc-close: true`"
         */
        
        /* 1 */
        $(document).on('click', '.'+selector.offcanvas_close, function(){
            Close($(this).parents('.'+selector.offcanvas));
        });

        /* 2 */
        $(document).on('click', selector.html, function(){
            if($(selector.html).find('.uis-offcanvas').hasClass('uis-open')){
                const option = OptionSeperator($(selector.html).find('.uis-offcanvas.uis-open').attr('uis-option'));
                for(let index = 0; index < option.length; index++){
                    // if the parameter `bg-close: true`
                    if(SplitParameter(option[index])[0] == 'bg-close' && SplitParameter(option[index])[1] == 'true'){
                        Close('#'+$(selector.html).find('.uis-offcanvas.uis-open').attr('id'));
                    }
                }
            }
        })

        // Adding some stopPropagation if user click inside the offcanvas-bar it will stop trigger the
        // closing of canvas because on `2`
        $(document).on('click', '.'+selector.bar, function(e){
            e.stopPropagation();
        });

        /* 4 */
        $(document).keyup(function(e){
            // check first if the key press is `Esc`
            if(e.key === "Escape"){
                // find offcanvas has a class of `uis-open`
                if($(selector.html).find('.uis-offcanvas').hasClass('uis-open')){
                    // split the seperators for multiple params
                    const option = OptionSeperator($(selector.html).find('.uis-offcanvas.uis-open').attr('uis-option'));
                    for(let index = 0; index < option.length; index++){
                        // if the parameter `esc-close: true`
                        if(SplitParameter(option[index])[0] == 'esc-close' && SplitParameter(option[index])[1] == 'true'){
                            Close('#'+$(selector.html).find('.uis-offcanvas.uis-open').attr('id'));
                        }
                    }
                }
            }
        });

        /**
         * 
         * @param {#id} id of the target offcanvas bar
         */
        const Open = function(target){
            const options = $(target).attr(selector.option);
            let settings;

            if(options != undefined){
                settings = OptionSeperator(options);
                if(settings.length >= 1){
                    for(let index = 0; index < settings.length; index++){

                        // Overlay modifier
                        if(SplitParameter(settings[index])[0] == 'overlay' && SplitParameter(settings[index])[1] == 'true'){
                            $(target).addClass(modifier.overlay);
                        }

                        //Flip modifier
                        else if(SplitParameter(settings[index])[0] == 'flip' && SplitParameter(settings[index])[1] == 'true'){
                            $(target).addClass(modifier.flip);
                            // we added flip class on the body to inform
                            // the `uis-offcanvas-container-animation` that we use flip in offcanvas-bar
                            $(selector.body).addClass(modifier.flip);
                        }
                        
                        // mode selection
                        else if(SplitParameter(settings[index])[0] == 'mode'){
                            if(SplitParameter(settings[index])[1] == 'slide'){
                                $(target).children().addClass(modifier.slide + ' ' +modifier.bar);
                            }
                            else if(SplitParameter(settings[index])[1] == 'push'){
                                $(target).children().addClass(modifier.push + ' ' + modifier.bar);
                                
                                setTimeout(function(){
                                    $(selector.body).addClass(modifier.container);
                                }, 50)
                            }
                            else if(SplitParameter(settings[index])[1] == 'none'){
                                $(target).children().addClass(modifier.none);
                            }
                            else{
                                console.error('please check your `mode: {param}` parameter');
                            }
                        }
                    }
                }
            }

            $(target).css('display', 'block');
            $(selector.html).css('overflow', 'hidden');
                           
            setTimeout(function(){
                $(selector.html).addClass(selector.page);
                $(selector.body).addClass(selector.container);

                $(target).addClass(state.open);
            }, 30);
        }

        /**
         * 
         * @param {#id} id of the target offcanvas bar
         */
        const Close = function(target){
            $(selector.html).removeClass(selector.page);

            $(target).removeClass(state.open);
            $(selector.body).removeClass(modifier.container);

            // 1. set delay to because we need to remove the `state.open` first
            // 2.  set delay to because we need to remove first the container-animation
            setTimeout(function(){
                /* 1 */
                $(target).css('display', '');
                $(selector.html).css('overflow', '');
                /* 2 */
                $(selector.body).removeClass(selector.container);
            }, 300);

            // Remove overlay modifier
            if($(target).hasClass(modifier.overlay)){
                setTimeout(function(){
                    $(target).removeClass(modifier.overlay);
                }, 300);
            }

            // Remove flip modifier
            if($(target).hasClass(modifier.flip)){
                setTimeout(function(){
                    $(target).removeClass(modifier.flip);
                    $(selector.body).removeClass(modifier.flip);
                }, 300);
            }

            // Remove mode modifier
            if($(target).children().hasClass(modifier.slide + ' ' + modifier.bar)){
                setTimeout(function(){
                    $(target).children().removeClass(modifier.slide + ' ' + modifier.bar);
                }, 300);
            }
            else if($(target).children().hasClass(modifier.push + ' ' + modifier.bar)){
                setTimeout(function(){
                    $(target).children().removeClass(modifier.push + ' ' + modifier.bar);
                }, 300);
            }
            else if($(target).children().hasClass(modifier.none)){
                $(target).children().removeClass(modifier.none);
            }
            
        }
    }();