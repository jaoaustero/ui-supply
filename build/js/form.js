    // ==================================================
    // Name:                Form
    // Description:         Scripts for form
    //
    // Component:           `[uis-form-customs]`
    //
    // ==================================================

    //  ==================================================
    //  Variables
    //  ==================================================

    $(document).on('change', '[type=file]', function(){
        $(this).siblings("[type=text]").val($(this).val());
    });