    // ==================================================
    // Component: Modal
    // ==================================================

    /**
     * Constants
     * ==================================================
     */

    const selector = {
        modal_attr          : '[uis-modal]',
        modal_attr_name     : 'uis-modal',
        modal_wrapper       : '.uis-modal',
        modal_dialog        : '.uis-modal-dialog',
        modal_close         : '.uis-close'
    };

    const state = {
        show        :   'open',
        hide        :   'close'
    };

    /**
     * Open Modal
     * 1. Trigger by click function
     * 2. Trigger by calling in another function `UIs.Modal(state: #ID)`
     */

    /* 1 */
    $(document).on('click', selector.modal_attr, function(){
        /**
         * A. Open using attribute parameter
         * B. Open using href parameter
         */
        if($(this).attr(selector.modal_attr_name) != ""){
            /* A */
            modal($(this).attr(selector.modal_attr_name));
        }else{
            /* B */
            modal('open: '+ window.location.hash);
        }
    });

    /* 2 */
    this.modal = function(data){
        modal(data);
    };



    /**
     * Close Modal
     * 1. Close by clicking the `.uis-close` class
     * 2. Close by clicking outside of the modal-dialog
     */

    /* 1 */
    $(document).on('click', selector.modal_close, function(){
        modal('close: #' + $(this).parents('.uis-modal').attr('id'));
    });

    /* 2 */
    $(document).on('click', selector.modal_wrapper, function(){
        modal('close: #' + $(this).attr('id'));
    });



    /**
     * Stop Event when user click on the dialog
     * because if we not set this it will close if the user clicks
     * on dialog cause by modal wrapper
     */
    $(document).on('click', selector.modal_dialog, e=>{
        e.stopPropagation();
    })


    /**
     * The parameter will pass by the trigger function
     * 1. Split the parameter `helper.js`
     * 2. Put the return value in varible object
     * 3. Function
     */
    function modal(data){
        /* 1 */
        const value = SplitParameter(data);
        
        /* 2 */
        const element = {
            state   : value[0],
            id      : value[1]
        };

        /* 3 */
        if(element.state == 'open'){
            /**
             * If `uis-modal` has `.uis-flex-top` 
             * We will set the display to flex to center the div
             */
            if(!$(element.id).hasClass('uis-flex-top')){
                $(element.id).css('display', 'block');
            }else{
                $(element.id).css('display', 'flex');
            }

            if($(element.id+'> .uis-modal-dialog > .uis-modal-body').attr('uis-overflow-auto') != undefined){
                const height = ($(selector.modal_wrapper).height() - 200);
                $(element.id+'> .uis-modal-dialog > .uis-modal-body').css({'min-height': '150px', 'height': height})
            }

            setTimeout(()=>{
                $(element.id).addClass('uis-open');
                $('html').addClass('uis-modal-page');
            }, 50);
        }else if(element.state == 'close'){
            $(element.id).removeClass('uis-open');
            $('html').removeClass('uis-modal-page');

            setTimeout(()=>{
                $(element.id).css('display', '');
            }, 300);
        }
    }
    