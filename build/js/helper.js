   
   
   
    /**
     * This function will split the 2 parameter
     * return e.g: [0]: "state", [1]: "#ID"  
     */
        
    const SplitParameter = param => {
        return param.replace(/ /g,"").split(":"); 
    };



    const OptionSeperator = param => {
        const options = param.replace(/ /g, "").split(";");

        return options;
    };
