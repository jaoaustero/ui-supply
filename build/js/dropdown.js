
    // ==================================================
    // Component: Nav
    // ==================================================

    /**
     * Constants
     * ==================================================
     */

    const Dropdown = function(){

        const selector = {
            attr       : '[uis-dropdown]',
            attr_name  : 'uis-dropdown',
            class      : '.uis-dropdown'
        }

        const state = {
            open    : 'uis-open'
        }

        $(document).on('mouseenter', '[uis-dropdown], .uis-dropdown', function(){
            $(this).siblings(selector.class)
                    .addClass(state.open);
        })
        .on('mouseleave', '[uis-dropdown], .uis-dropdown', function(){
            // $(this).siblings(selector.class)
            //         .removeClass(state.open);
        });

    }();