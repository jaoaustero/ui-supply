    // ==================================================
    // Component: Notification
    // ==================================================
   
    this.Notification = function(data, options){
        /**
         * Constants
         * ==================================================
         */

        const selector = {
            body        : 'body',
            container   : '.uis-notification'
        }

        // Default settings for notification
        let settings = {
            message     : "Message",
            status      : "default",
            icon        : null,
            position    : "top-center",
            timeout     : 3000
        }



        /**
         * Give the element time out
         */
        function AssignTimeOut(element, timeout){
            setTimeout(function(){
                element.fadeOut();
            }, timeout);
        }


        /**
         * Condition
         * 1. Check if parameter is object only
         * 2. Check if `data` param is string only
         * 3. 1st param striing, 2nd param object for options
         */

        if(typeof(data) == "object" && options == undefined){
            settings.message = data.message != null ? data.message : settings.message;
            settings.status = data.status != null ? data.status : settings.status;
            settings.icon = data.icon != null ? data.icon : settings.icon;
            settings.position = data.position != null ? data.position : settings.position;
            settings.timeout = data.timeout != null ? data.timeout : settings.timeout;

            CreateElement(settings);

        }else if(typeof(data) == "string" && options == undefined){
            settings.message = data;

            CreateElement(settings);
        }else if(typeof(data) == "string" && options != undefined){            
            settings.message = data;
            settings.status = options.status != null ? options.status : settings.status;
            settings.icon = options.icon != null ? options.icon : settings.icon;
            settings.position = options.position != null ? options.position : settings.position;
            settings.timeout = options.timeout != null ? options.timeout : settings.timeout;

            CreateElement(settings);
        }else{
            console.error('Please check your Javascript API');
        }


        /**
         * DOM Element
         * @param {object} object 
         */
        function CreateElement(value){
            $uis_container = $("<div class='uis-notification uis-notification-"+ value.position +" uis-open' />");

            $notification_container = $("<div class='uis-notification-message uis-notification-with-icon uis-notification-message-"+ value.status +"' />");
            
            // Side Icon
            if(value.icon != null){
                $notification_icon_container = $("<div class='uis-notification-icon'>");
                $notification_icon = $("<i class='li li-"+ value.icon +" li-lg'>");

                $notification_icon_container.append($notification_icon);

                $notification_container.append($notification_icon_container);
            }
            
    
            // Content
            $notification_content = $("<div class='uis-notification-content'>");
            $notification_content.text(value.message);
    
            $notification_button_close = $("<button class='uis-close uis-close-inverse uis-notification-close uis-notification-button-close' uis-close>")
            $notification_button_close_icon = $("<i class='li li-close'>")
    
            $notification_button_close.append($notification_button_close_icon)
    
            $notification_container.append($notification_content);
            $notification_container.append($notification_button_close);
    
            $uis_container.append($notification_container);
            
            if($(selector.body).find(selector.container).length != 0){
                if($('body').find('.uis-notification-'+value.position).length != 0){
                    $('.uis-notification-'+value.position).prepend($notification_container);
                }else{
                    console.log('else');
                    $(selector.body).prepend($uis_container);
                }
            }else{
                $(selector.body).prepend($uis_container);
            }
    
            AssignTimeOut($notification_container, value.timeout);
        }
    };