var UIsupply = function(){
    // ==================================================
    // Component: Accordion
    // ==================================================

    $('.uis-accordion > li').on('click', function(){
        if($(this).children('.uis-accordion-content').hasClass('uis-open')){
            $(this).children('.uis-accordion-content').slideToggle().removeClass('uis-open');
            $(this).find('a > i').removeClass('accordion-icon-rotate');
        }else{
            $(this).children('.uis-accordion-content').slideToggle().addClass('uis-open');
            $(this).find('a > i').addClass('accordion-icon-rotate');
        }
    });
    // ==================================================
    // Component: Alert
    // ==================================================

    // class Alert{
    //     constructor(){
    //         this.onload();
    //     }

    //     onload(){
    //         console.log('All onload functions goes here');
    //         // this.showit("I'm showit function access by onload function");
    //     }

    //     showit(data){
    //         console.log(data);
    //     }
    // }

    // export default Alert;

    // ==================================================
    // Component: Nav
    // ==================================================

    /**
     * Constants
     * ==================================================
     */

    const Dropdown = function(){

        const selector = {
            attr       : '[uis-dropdown]',
            attr_name  : 'uis-dropdown',
            class      : '.uis-dropdown'
        }

        const state = {
            open    : 'uis-open'
        }

        $(document).on('mouseenter', '[uis-dropdown], .uis-dropdown', function(){
            $(this).siblings(selector.class)
                    .addClass(state.open);
        })
        .on('mouseleave', '[uis-dropdown], .uis-dropdown', function(){
            // $(this).siblings(selector.class)
            //         .removeClass(state.open);
        });

    }();
    // ==================================================
    // Name:                Form
    // Description:         Scripts for form
    //
    // Component:           `[uis-form-customs]`
    //
    // ==================================================

    //  ==================================================
    //  Variables
    //  ==================================================

    $(document).on('change', '[type=file]', function(){
        $(this).siblings("[type=text]").val($(this).val());
    });
   
   
   
    /**
     * This function will split the 2 parameter
     * return e.g: [0]: "state", [1]: "#ID"  
     */
        
    const SplitParameter = param => {
        return param.replace(/ /g,"").split(":"); 
    };



    const OptionSeperator = param => {
        const options = param.replace(/ /g, "").split(";");

        return options;
    };

    // ==================================================
    // Component: Modal
    // ==================================================

    /**
     * Constants
     * ==================================================
     */

    const selector = {
        modal_attr          : '[uis-modal]',
        modal_attr_name     : 'uis-modal',
        modal_wrapper       : '.uis-modal',
        modal_dialog        : '.uis-modal-dialog',
        modal_close         : '.uis-close'
    };

    const state = {
        show        :   'open',
        hide        :   'close'
    };

    /**
     * Open Modal
     * 1. Trigger by click function
     * 2. Trigger by calling in another function `UIs.Modal(state: #ID)`
     */

    /* 1 */
    $(document).on('click', selector.modal_attr, function(){
        /**
         * A. Open using attribute parameter
         * B. Open using href parameter
         */
        if($(this).attr(selector.modal_attr_name) != ""){
            /* A */
            modal($(this).attr(selector.modal_attr_name));
        }else{
            /* B */
            modal('open: '+ window.location.hash);
        }
    });

    /* 2 */
    this.modal = function(data){
        modal(data);
    };



    /**
     * Close Modal
     * 1. Close by clicking the `.uis-close` class
     * 2. Close by clicking outside of the modal-dialog
     */

    /* 1 */
    $(document).on('click', selector.modal_close, function(){
        modal('close: #' + $(this).parents('.uis-modal').attr('id'));
    });

    /* 2 */
    $(document).on('click', selector.modal_wrapper, function(){
        modal('close: #' + $(this).attr('id'));
    });



    /**
     * Stop Event when user click on the dialog
     * because if we not set this it will close if the user clicks
     * on dialog cause by modal wrapper
     */
    $(document).on('click', selector.modal_dialog, e=>{
        e.stopPropagation();
    })


    /**
     * The parameter will pass by the trigger function
     * 1. Split the parameter `helper.js`
     * 2. Put the return value in varible object
     * 3. Function
     */
    function modal(data){
        /* 1 */
        const value = SplitParameter(data);
        
        /* 2 */
        const element = {
            state   : value[0],
            id      : value[1]
        };

        /* 3 */
        if(element.state == 'open'){
            /**
             * If `uis-modal` has `.uis-flex-top` 
             * We will set the display to flex to center the div
             */
            if(!$(element.id).hasClass('uis-flex-top')){
                $(element.id).css('display', 'block');
            }else{
                $(element.id).css('display', 'flex');
            }

            if($(element.id+'> .uis-modal-dialog > .uis-modal-body').attr('uis-overflow-auto') != undefined){
                const height = ($(selector.modal_wrapper).height() - 200);
                $(element.id+'> .uis-modal-dialog > .uis-modal-body').css({'min-height': '150px', 'height': height})
            }

            setTimeout(()=>{
                $(element.id).addClass('uis-open');
                $('html').addClass('uis-modal-page');
            }, 50);
        }else if(element.state == 'close'){
            $(element.id).removeClass('uis-open');
            $('html').removeClass('uis-modal-page');

            setTimeout(()=>{
                $(element.id).css('display', '');
            }, 300);
        }
    }
    

    // ==================================================
    // Component: Nav
    // ==================================================

    const Nav = function(){
        /**
         * Constants
         * ==================================================
         */
        const selector = {
            nav_attr        : '[uis-nav-accordion]',
            nav_attr_name   :   'uis-nav-accordion'
        }

        const state = {
            open    : 'uis-open'
        }

        /**
         * Nav Accordion
         * When user clicks on the parent
         * 1. If the parent element has `href` it will prevent it.
         * 2. Get the attribute value
         * 3. Condition
         */
        $(document).on('click', selector.nav_attr + '> .uis-parent', function(e){
            /* 1 */
            e.preventDefault();
            
            /* 2 */
            const value = SplitParameter($(this).parent('ul.uis-nav').attr(selector.nav_attr_name));
            
            /* 3 */
            if(value[0] == 'multiple' && value[1] == 'true'){
                toggleNavSub($(this))
            }else{
                /**
                 * close the other siblings
                 */
                if($(this).siblings().hasClass(state.open)){
                    $(this).siblings().removeClass(state.open);
                    $(this).siblings().children('ul').slideToggle();
                }
                toggleNavSub($(this))
            }
        })

        function toggleNavSub(target){
            if(!$(target).hasClass(state.open)){
                $(target).addClass(state.open);
            }else{
                $(target).removeClass(state.open);
            }

            $(target).children('ul.uis-nav-sub').slideToggle();
        }
    }();

    // ==================================================
    // Component: Notification
    // ==================================================
   
    this.Notification = function(data, options){
        /**
         * Constants
         * ==================================================
         */

        const selector = {
            body        : 'body',
            container   : '.uis-notification'
        }

        // Default settings for notification
        let settings = {
            message     : "Message",
            status      : "default",
            icon        : null,
            position    : "top-center",
            timeout     : 3000
        }



        /**
         * Give the element time out
         */
        function AssignTimeOut(element, timeout){
            setTimeout(function(){
                element.fadeOut();
            }, timeout);
        }


        /**
         * Condition
         * 1. Check if parameter is object only
         * 2. Check if `data` param is string only
         * 3. 1st param striing, 2nd param object for options
         */

        if(typeof(data) == "object" && options == undefined){
            settings.message = data.message != null ? data.message : settings.message;
            settings.status = data.status != null ? data.status : settings.status;
            settings.icon = data.icon != null ? data.icon : settings.icon;
            settings.position = data.position != null ? data.position : settings.position;
            settings.timeout = data.timeout != null ? data.timeout : settings.timeout;

            CreateElement(settings);

        }else if(typeof(data) == "string" && options == undefined){
            settings.message = data;

            CreateElement(settings);
        }else if(typeof(data) == "string" && options != undefined){            
            settings.message = data;
            settings.status = options.status != null ? options.status : settings.status;
            settings.icon = options.icon != null ? options.icon : settings.icon;
            settings.position = options.position != null ? options.position : settings.position;
            settings.timeout = options.timeout != null ? options.timeout : settings.timeout;

            CreateElement(settings);
        }else{
            console.error('Please check your Javascript API');
        }


        /**
         * DOM Element
         * @param {object} object 
         */
        function CreateElement(value){
            $uis_container = $("<div class='uis-notification uis-notification-"+ value.position +" uis-open' />");

            $notification_container = $("<div class='uis-notification-message uis-notification-with-icon uis-notification-message-"+ value.status +"' />");
            
            // Side Icon
            if(value.icon != null){
                $notification_icon_container = $("<div class='uis-notification-icon'>");
                $notification_icon = $("<i class='li li-"+ value.icon +" li-lg'>");

                $notification_icon_container.append($notification_icon);

                $notification_container.append($notification_icon_container);
            }
            
    
            // Content
            $notification_content = $("<div class='uis-notification-content'>");
            $notification_content.text(value.message);
    
            $notification_button_close = $("<button class='uis-close uis-close-inverse uis-notification-close uis-notification-button-close' uis-close>")
            $notification_button_close_icon = $("<i class='li li-close'>")
    
            $notification_button_close.append($notification_button_close_icon)
    
            $notification_container.append($notification_content);
            $notification_container.append($notification_button_close);
    
            $uis_container.append($notification_container);
            
            if($(selector.body).find(selector.container).length != 0){
                if($('body').find('.uis-notification-'+value.position).length != 0){
                    $('.uis-notification-'+value.position).prepend($notification_container);
                }else{
                    console.log('else');
                    $(selector.body).prepend($uis_container);
                }
            }else{
                $(selector.body).prepend($uis_container);
            }
    
            AssignTimeOut($notification_container, value.timeout);
        }
    };
    // ==================================================
    // Component: Offcanvas
    // ==================================================

    this.Offcanvas = function(){
        /**
         * Constants
         * ==================================================
         */
        const selector = {
            attr            : '[uis-offcanvas]',
            attr_name       : 'uis-offcanvas',
            bar             : 'uis-offcanvas-bar',
            container       : 'uis-offcanvas-container',
            page            : 'uis-offcanvas-page',
            offcanvas       : 'uis-offcanvas',

            option_attr     : '[uis-option]',
            option          : 'uis-option',

            offcanvas_close : 'uis-offcanvas-close',

            html            : 'html',
            body            : 'body'
        }

        const modifier = {
            bar                 : 'uis-offcanvas-bar-animation',
            container           : 'uis-offcanvas-container-animation',
            overlay             : 'uis-offcanvas-overlay',
            flip                : 'uis-offcanvas-flip',
            slide               : 'uis-offcanvas-slide',
            push                : 'uis-offcanvas-push',
            none                : 'uis-offcanvas-none'
        }
    
        const state = {
            open        : 'uis-open'
        }
    
        /**
         * The parameter will pass by the trigger function
         * 1. Split the parameter `helper.js`
         * 2. Put the return value in varible object
         * 3. Function
         */

        $(document).on('click', selector.attr, function(){
            
            /**
             * Note: To trigger this function you need to include the `uis-offcanvas as your attribute`
             * Two options for opening the offcanvas
             * A. Using by anchor using the attr `uis-offcanvas` it will get the `href` value.
             * B. Using by button it will get the value of `uis-offcanvas` attr.
             */

            /* A */
            if($(this).attr('href')){
                Open($(this).attr('href'));
            }
            /* B */
            else{
                /* 1 */
                const value = SplitParameter($(this).attr(selector.attr_name));

                /* 2 */
                const element = {
                    action  : value[0],
                    target  : value[1]
                };

                if(element.action == 'target'){
                    Open(element.target);
                }
            }
        });

        /*
         * Close sidebar
         * 1. Trigger by `uis-offcanvas-close` button
         * 2. Trigger by clicking outside of the offcanvas bar "If param has `bg-close: true`"
         * 3. Trigger by calling the function
         * 4. Trigger by pressing the `Esc` key "If param has `esc-close: true`"
         */
        
        /* 1 */
        $(document).on('click', '.'+selector.offcanvas_close, function(){
            Close($(this).parents('.'+selector.offcanvas));
        });

        /* 2 */
        $(document).on('click', selector.html, function(){
            if($(selector.html).find('.uis-offcanvas').hasClass('uis-open')){
                const option = OptionSeperator($(selector.html).find('.uis-offcanvas.uis-open').attr('uis-option'));
                for(let index = 0; index < option.length; index++){
                    // if the parameter `bg-close: true`
                    if(SplitParameter(option[index])[0] == 'bg-close' && SplitParameter(option[index])[1] == 'true'){
                        Close('#'+$(selector.html).find('.uis-offcanvas.uis-open').attr('id'));
                    }
                }
            }
        })

        // Adding some stopPropagation if user click inside the offcanvas-bar it will stop trigger the
        // closing of canvas because on `2`
        $(document).on('click', '.'+selector.bar, function(e){
            e.stopPropagation();
        });

        /* 4 */
        $(document).keyup(function(e){
            // check first if the key press is `Esc`
            if(e.key === "Escape"){
                // find offcanvas has a class of `uis-open`
                if($(selector.html).find('.uis-offcanvas').hasClass('uis-open')){
                    // split the seperators for multiple params
                    const option = OptionSeperator($(selector.html).find('.uis-offcanvas.uis-open').attr('uis-option'));
                    for(let index = 0; index < option.length; index++){
                        // if the parameter `esc-close: true`
                        if(SplitParameter(option[index])[0] == 'esc-close' && SplitParameter(option[index])[1] == 'true'){
                            Close('#'+$(selector.html).find('.uis-offcanvas.uis-open').attr('id'));
                        }
                    }
                }
            }
        });

        /**
         * 
         * @param {#id} id of the target offcanvas bar
         */
        const Open = function(target){
            const options = $(target).attr(selector.option);
            let settings;

            if(options != undefined){
                settings = OptionSeperator(options);
                if(settings.length >= 1){
                    for(let index = 0; index < settings.length; index++){

                        // Overlay modifier
                        if(SplitParameter(settings[index])[0] == 'overlay' && SplitParameter(settings[index])[1] == 'true'){
                            $(target).addClass(modifier.overlay);
                        }

                        //Flip modifier
                        else if(SplitParameter(settings[index])[0] == 'flip' && SplitParameter(settings[index])[1] == 'true'){
                            $(target).addClass(modifier.flip);
                            // we added flip class on the body to inform
                            // the `uis-offcanvas-container-animation` that we use flip in offcanvas-bar
                            $(selector.body).addClass(modifier.flip);
                        }
                        
                        // mode selection
                        else if(SplitParameter(settings[index])[0] == 'mode'){
                            if(SplitParameter(settings[index])[1] == 'slide'){
                                $(target).children().addClass(modifier.slide + ' ' +modifier.bar);
                            }
                            else if(SplitParameter(settings[index])[1] == 'push'){
                                $(target).children().addClass(modifier.push + ' ' + modifier.bar);
                                
                                setTimeout(function(){
                                    $(selector.body).addClass(modifier.container);
                                }, 50)
                            }
                            else if(SplitParameter(settings[index])[1] == 'none'){
                                $(target).children().addClass(modifier.none);
                            }
                            else{
                                console.error('please check your `mode: {param}` parameter');
                            }
                        }
                    }
                }
            }

            $(target).css('display', 'block');
            $(selector.html).css('overflow', 'hidden');
                           
            setTimeout(function(){
                $(selector.html).addClass(selector.page);
                $(selector.body).addClass(selector.container);

                $(target).addClass(state.open);
            }, 30);
        }

        /**
         * 
         * @param {#id} id of the target offcanvas bar
         */
        const Close = function(target){
            $(selector.html).removeClass(selector.page);

            $(target).removeClass(state.open);
            $(selector.body).removeClass(modifier.container);

            // 1. set delay to because we need to remove the `state.open` first
            // 2.  set delay to because we need to remove first the container-animation
            setTimeout(function(){
                /* 1 */
                $(target).css('display', '');
                $(selector.html).css('overflow', '');
                /* 2 */
                $(selector.body).removeClass(selector.container);
            }, 300);

            // Remove overlay modifier
            if($(target).hasClass(modifier.overlay)){
                setTimeout(function(){
                    $(target).removeClass(modifier.overlay);
                }, 300);
            }

            // Remove flip modifier
            if($(target).hasClass(modifier.flip)){
                setTimeout(function(){
                    $(target).removeClass(modifier.flip);
                    $(selector.body).removeClass(modifier.flip);
                }, 300);
            }

            // Remove mode modifier
            if($(target).children().hasClass(modifier.slide + ' ' + modifier.bar)){
                setTimeout(function(){
                    $(target).children().removeClass(modifier.slide + ' ' + modifier.bar);
                }, 300);
            }
            else if($(target).children().hasClass(modifier.push + ' ' + modifier.bar)){
                setTimeout(function(){
                    $(target).children().removeClass(modifier.push + ' ' + modifier.bar);
                }, 300);
            }
            else if($(target).children().hasClass(modifier.none)){
                $(target).children().removeClass(modifier.none);
            }
            
        }
    }();
} 

var UIs = new UIsupply();